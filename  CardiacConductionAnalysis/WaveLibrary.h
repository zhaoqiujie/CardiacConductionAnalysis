#pragma once


#define DIPOLE_MAX      200           //定义最大偶极子数组数量

typedef struct __tag_DIPOLE
{
	double dfRadius;                  //等效偶极子的半径
	double dfTheta;                   //等效偶极子的角度
} DIPOLE;

class WaveLibrary
{
public:
	WaveLibrary(void);
	virtual ~WaveLibrary(void);
};

