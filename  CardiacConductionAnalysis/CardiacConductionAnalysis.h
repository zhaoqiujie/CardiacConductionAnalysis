
// CardiacConductionAnalysis.h : CardiacConductionAnalysis 应用程序的主头文件
//
#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"       // 主符号


// MainApp:
// 有关此类的实现，请参阅 CardiacConductionAnalysis.cpp
//
class CCADoc;
class CCAView;
class CardiacConfig;
class MainApp : public CWinApp
{
public:
	CCADoc*  m_pDoc;
	CCAView* m_pView;

public:
	MainApp();

private:
	HANDLE m_hMutex;

// 重写
public:
	virtual BOOL InitInstance();

// 实现
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern MainApp theApp;
