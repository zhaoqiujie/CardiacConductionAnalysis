#pragma once
#include "afxwin.h"

class CSmartDC;
class SketchImage : public CStatic
{
public:
	int m_w;
	int m_h;
	CSmartDC* m_pdcSketch;

public:
	SketchImage();
	void SetSketchDC(CSmartDC* pdc, int w, int h);
	virtual ~SketchImage(void);
	virtual void PreSubclassWindow();
	virtual void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);
};

